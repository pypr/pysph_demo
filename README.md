# PySPH Binder Demo Repository

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/pypr%2Fpysph_demo/HEAD)

Click the above launch binder icon to fire up a binder instance on this
repository. This gives you a working PySPH installation along with some sample
simulation data in one, two, and three dimensions that you can explore
entirely on the web.

This is a demonstration of the convenient binder support that PySPH provides.
The directory contains the scripts used to generate output data and Jupyter
notebook files in each of the output directories that can be used to view the
data.

This repository is a companion to the paper https://arxiv.org/abs/1909.04504
and the repository https://gitlab.com/pypr/pysph_paper


The `db2d_output` directory has the output from the `db2d.py` example. The
`dam_break_3d_output` contains output from a simulation made using
`dam_break_3d.py` but executed on a GPU. The `sod_shocktube_output` contains
results of a 1D simulation.

Each of these directories has a jupyter notebook file that can be used to
visualize the data online. Feel free to try it. You can also write your own
Python code in the notebook to explore the data.

See https://pysph.readthedocs.io/ for more information on PySPH.


## Generating the data yourself

If you are curious how we generated this data, one can do the following with
the current (as of December 30th, 2020) PySPH master branch assuming that you
have PySPH.

The 1D example data was created using:
```
$ pysph run gas_dynamics.sod_shocktube --openmp
```
and it runs in less than 10 seconds.

The simple 2D example runs in less than 30 seconds:

```
$ python db2d.py --openmp

```

The 3D example takes a while and is much faster on a GPU, otherwise it may
take close to 30 minutes on a quad-core computer as follows:

```
$ python dam_break_3d.py --pfreq 100 --tf 2.0 -z --cache-nnps --opencl
```
The `-z` argument above produces compressed HDF5 files which reduce the file
size by a factor of 5.

If there are too many files and you do not want to upload that many, you can
cull the output files using:

```
$ pysph cull -c 2 dam_break_3d_output

```

this retains one in 2 files (make a copy if you want those files locally).
Learn more using `pysph cull -h`.

Now that these are complete, we just run the following:

```
$ pysph binder .
```

Which produces a simple `README.md` that you should edit. It also scans all
output directories and creates suitable jupyter notebook files there that
people can use to view the data.

Once this is done, you can just upload this to github or gitlab, remember to
change the URLs suitably.
